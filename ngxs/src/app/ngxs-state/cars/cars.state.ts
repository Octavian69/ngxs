import { Car } from "src/app/models/Car";
import { State, Selector, Action, StateContext } from "@ngxs/store";
import { LoadCars, AddCar, RemoveCar, UpdateCar } from "./cars.actions";


interface CarsStateModel {
    cars: Car[]
}


@State<CarsStateModel>({
    name: 'cars',
    defaults: {
        cars: []
    }
})
export class CarsState {
    
    @Selector()
    static getCars(state: CarsStateModel) {
        return state.cars;
    }

    @Action(LoadCars)
        load({ setState }: StateContext<CarsStateModel>, { payload }: LoadCars) {
            return setState({
                cars: [...payload]
            })
        }

    @Action(AddCar)
        add({getState, patchState}: StateContext<CarsStateModel>, {payload}: AddCar) {
            const state = getState();

            return patchState({
                ...state,
                cars: [...state.cars, payload]
            })
        }

    @Action(RemoveCar)
        remove({getState, patchState}: StateContext<CarsStateModel>, { payload }: RemoveCar) {
            const state  = getState();
            const update = state.cars.filter(car => car.id !== payload.id);

            return patchState({
                ...state,
                cars: [... update]
            })
        }
    
    @Action(UpdateCar)
        update({ getState, setState }: StateContext<CarsStateModel>, { payload }: UpdateCar) {
            const state = getState();
            const index = state.cars.findIndex(car => car.id === payload.id);

            state.cars[index] = payload;

            return setState({
                cars: [...state.cars]
            })
        }
}