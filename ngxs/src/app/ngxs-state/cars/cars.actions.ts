import { Car } from "src/app/models/Car";

export class LoadCars {
    static readonly type = '[CARS] Load'

    constructor(public payload: Car[]){}
}

export class AddCar {
    static readonly type = '[CARS] Add';

    constructor(public payload: Car) {}
}

export class RemoveCar {
    static readonly type = '[CARS] Remove';
    
    constructor(public payload: Car) {}
}

export class UpdateCar {
    static readonly type = '[CARS] Update';

    constructor(public payload: Car) {}
}
