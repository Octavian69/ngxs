import { Component, OnInit, Input } from '@angular/core';
import { Car } from '../models/Car';
import { CarsService } from '../services/cars.service';



@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @Input('car') car: Car;
  

  constructor(
    public carsService: CarsService
  ) { }


  ngOnInit() {
  }

  public removeCar(): void {
    this.carsService.removeCar(this.car);
  }

  public buyCar(): void {
    const update = Object.assign({}, this.car, { isSold: true });

    this.carsService.updateCar(update);
  }

}
