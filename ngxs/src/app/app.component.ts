import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { CarsState } from './ngxs-state/cars/cars.state';
import { Car } from './models/Car';
import { Observable } from 'rxjs';
import { CarsService } from './services/cars.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  @Select(CarsState.getCars) cars$: Observable<Car[]>;

  constructor(
    public carsService: CarsService
  ) {}

  ngOnInit() {
    
  }

  loadCars(): void {
    this.carsService.loadCars();
  }
}
