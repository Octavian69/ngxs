import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { CarsService } from '../services/cars.service';
import { Car } from '../models/Car';

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  form: FormGroup;

  @Output('loadCars') load = new EventEmitter<void>();

  constructor(
    private fb: FormBuilder,
    public carsService: CarsService
    ) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      model: ['', [Validators.required]]
    })
  }
  
  onSubmit() {
    const {name, model} = this.form.value;
    const date = moment().format('DD.MM.YYYY');

    this.carsService.addCar(new Car(name, model, date));

    this.form.reset();
  }

  loadCars(): void {
    this.load.emit();
  }
}
