import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { Car } from "../models/Car";
import { LoadCars, AddCar, RemoveCar, UpdateCar } from "../ngxs-state/cars/cars.actions";
import { delay } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class CarsService {

    static BASE_CARS_URL = 'http://localhost:3000/cars/';

    _isDisabled: boolean = false;

    get isDisabled(): boolean {
        return this._isDisabled;
    }
    
    
    constructor(
        private http: HttpClient,
        private store: Store
    ) {}

    preloadCars(): Observable<Car[]> {
        return this.http.get<Car[]>(CarsService.BASE_CARS_URL).pipe(delay(2000));
    }

    loadCars(): void {
        this._isDisabled = true;

        this.preloadCars().subscribe((cars: Car[]) => {
            this.store.dispatch(new LoadCars(cars));

            this._isDisabled = false;
        })
    }

    addCar(car: Car): void {
        this.http.post(CarsService.BASE_CARS_URL, car).subscribe((car: Car) => {

            this.store.dispatch(new AddCar(car));

        })
    }

    removeCar(car: Car): void {
        this.http.delete(CarsService.BASE_CARS_URL + car.id).subscribe((response: Response) => {

            this.store.dispatch(new RemoveCar(car));

        })
    }

    updateCar(car: Car): void {
        this.http.patch(CarsService.BASE_CARS_URL + car.id, car).subscribe((updateCar: Car) => {

            this.store.dispatch(new UpdateCar(updateCar));

        })
    }
}